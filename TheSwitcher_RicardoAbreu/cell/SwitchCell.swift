//
//  SwitchCell.swift
//  TheSwitcher_RicardoAbreu
//
//  Created by Ricardo Abreu on 25/05/2020.
//  Copyright © 2020 Ricardo Abreu. All rights reserved.
//

import UIKit

class SwitchCell: UITableViewCell {

    private var device: Device? = nil
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var switchLight: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setup(device: Device) {
        self.device = device
        
        labelTitle.text = device.name
        switchLight.isOn = device.isOn
        
        switchLight.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
    }
    
    @objc func switchChanged(mySwitch: UISwitch) {
        self.device?.isOn = mySwitch.isOn
    }
}
