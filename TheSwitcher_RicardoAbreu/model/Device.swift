//
//  Device.swift
//  TheSwitcher_RicardoAbreu
//
//  Created by Ricardo Abreu on 25/05/2020.
//  Copyright © 2020 Ricardo Abreu. All rights reserved.
//

import Foundation

class Device {
    
    let name: String
    var isOn: Bool
    
    init(name: String, isOn: Bool) {
        self.name = name
        self.isOn = isOn
    }
}
