//
//  DivisionDetailsVC.swift
//  TheSwitcher_RicardoAbreu
//
//  Created by Ricardo Abreu on 25/05/2020.
//  Copyright © 2020 Ricardo Abreu. All rights reserved.
//

import UIKit

class DivisionDetailsVC: UIViewController {

    var device: Device? = nil
    
    @IBOutlet weak var imageViewDeviceState: UIImageView!
    @IBOutlet weak var labelStateText: UILabel!
    @IBOutlet weak var labelStateValue: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if device!.isOn {
            imageViewDeviceState.image = UIImage(named: "ic_light_on")
            labelStateValue.text = "ON"
        } else {
            imageViewDeviceState.image = UIImage(named: "ic_light_off")
            labelStateValue.text = "OFF"
        }
        
        labelStateText.text = "Your \(device!.name) light is"
    }
}
