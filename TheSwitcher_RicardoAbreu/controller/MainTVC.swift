//
//  MainTVC.swift
//  TheSwitcher_RicardoAbreu
//
//  Created by Ricardo Abreu on 25/05/2020.
//  Copyright © 2020 Ricardo Abreu. All rights reserved.
//

import UIKit

class MainTVC: UITableViewController {

    private var data = [Device]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "SwitchCell", bundle: nil), forCellReuseIdentifier: "switchCell")
        
        data.append(Device(name: "Kitchen", isOn: false))
        data.append(Device(name: "Living room", isOn: false))
        data.append(Device(name: "Master bedroom", isOn: false))
        data.append(Device(name: "Guest's bedroom", isOn: false))
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SwitchCell = tableView.dequeueReusableCell(withIdentifier: "switchCell", for: indexPath) as! SwitchCell
        cell.setup(device: data[indexPath.row])
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "segueDivisionDetailsVC", sender: data[indexPath.row])
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueDivisionDetailsVC" {
            let destination = segue.destination as! DivisionDetailsVC
            destination.device = (sender as! Device)
        }
    }
}
